import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestDemo {
    @Test
    void sampleTest() {
        assertThat(3, is(equalTo(3)));
    }
}
